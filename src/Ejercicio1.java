import java.io.*;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Ejercicio1 {
    public static void main(String[] args) {
        List<String> argList = new ArrayList<>(Arrays.stream(args).toList());
        ProcessBuilder pb = new ProcessBuilder(argList);
        File out = new File("output.txt");
        try {
            Process pro = pb.start();
            if (!pro.waitFor(2, TimeUnit.SECONDS)){
                pro.destroy();
                throw new InterruptedException();
            }
            int salida = pro.waitFor();
            BufferedWriter bw = new BufferedWriter(new FileWriter(out));
            Scanner proSC = new Scanner(pro.getInputStream());
            Scanner proErr = new Scanner(pro.getErrorStream());
            String linea;
            System.out.println("Valor de salida de proceso hijo: " + salida);
            if (salida != 0){
                while (proErr.hasNextLine()) {
                    linea = proSC.nextLine();
                    System.out.println(linea);
                }
            }else {
                while (proSC.hasNextLine()) {
                    linea = proSC.nextLine();
                    System.out.println(linea);
                    bw.write(linea + "\n");
                }
                bw.close();
            }
        }catch (IOException e) {
            System.out.println("No se ha podido ejecutar el proceso");
        }catch (InterruptedException e) {
            System.out.println("Tiempo de espera excedido");
        }
    }
}
