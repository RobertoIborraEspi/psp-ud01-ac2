import java.io.*;
import java.util.*;

public class Ejercicio2 {
    public static void main(String[] args) {
        String comando = "java -jar C:\\Users\\TheMostLoko\\IdeaProjects\\PSP-UD01-AC2\\Random10.jar";
        List<String> list = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(list);
        //pb.inheritIO();
        //pb.redirectOutput(ProcessBuilder.Redirect.INHERIT);
        try {
            Process p = pb.start();
            InputStream is = p.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            OutputStream os = p.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedReader br = new BufferedReader(isr);
            BufferedWriter bw = new BufferedWriter(osw);
            BufferedWriter bwf = new BufferedWriter(new FileWriter("randoms.txt"));
            Scanner sc = new Scanner(System.in);
            StringBuilder sb = new StringBuilder();
            String linea = sc.nextLine();
            while (!Objects.equals(linea, "stop")) {
                bw.write(linea + "\n");
                bw.flush();
                linea = br.readLine();
                sb.append(linea).append("\n");
                System.out.println(linea);
                linea = sc.nextLine();
            }
            bwf.write(sb.toString());
            bwf.close();
            bw.close();
            br.close();
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
