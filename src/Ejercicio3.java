import java.io.*;
import java.util.*;

public class Ejercicio3 {
    public static void main(String[] args) {
        String comando = "java -jar C:\\Users\\TheMostLoko\\IdeaProjects\\PSP-UD01-AC2\\Minusculas.jar";
        List<String> list = new ArrayList<>(Arrays.asList(comando.split(" ")));
        ProcessBuilder pb = new ProcessBuilder(list);
        try {
            Process p = pb.start();
            InputStream is = p.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            OutputStream os = p.getOutputStream();
            OutputStreamWriter osw = new OutputStreamWriter(os);
            BufferedReader br = new BufferedReader(isr);
            BufferedWriter bw = new BufferedWriter(osw);
            Scanner sc = new Scanner(System.in);
            String linea = sc.nextLine();
            while (!Objects.equals(linea, "finalizar")) {
                bw.write(linea + "\n");
                bw.flush();
                linea = br.readLine();
                System.out.println(linea);
                linea = sc.nextLine();
            }
            bw.close();
            br.close();
        }catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
